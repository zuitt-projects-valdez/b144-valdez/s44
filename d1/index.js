//get post data
fetch("https://jsonplaceholder.typicode.com/posts")
  .then((response) => response.json())
  .then((data) => showPosts(data)); //placeholder server; fetch returns a promise

//add post data
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
  //prevents the page from loading
  e.preventDefault();

  //need to fetch the server again
  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: document.querySelector("#txt-title").value,
      body: document.querySelector("#txt-body").value,
      userId: 1,
    }), //different from body property, refers to the whole post as a body; body structure
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => response.json()) //returning response in JSON data
    .then((data) => {
      console.log(data); //server won't add to their database so can prove that the method worked through the console
      alert("Successfully added");

      //clears the text elements upon post creation
      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-body").value = null;
    });
});

//Show posts
const showPosts = (posts) => {
  let postEntries = "";
  posts.forEach((post) => {
    postEntries += `
    <div id="post-${post.id}">
      <h3 id="post-title-${post.id}">${post.title}</h3>
      <p id="post-body-${post.id}">${post.body}</p>
      <button onclick="editPost('${post.id}')">Edit</button>
      <button onclick="deletePost('${post.id}')">Delete</button>
    </div>
    `;
  });
  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

//edit post
const editPost = (id) => {
  //retrieiving specific post you want to edit
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  //remove disabled attribute from the button
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

//update post
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
  e.preventDefault();

  fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    body: JSON.stringify({
      id: document.querySelector("#txt-edit-id").value,
      title: document.querySelector("#txt-edit-title").value,
      body: document.querySelector("#txt-edit-body").value,
      userId: 1,
    }),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully updated.");

      document.querySelector("#txt-edit-id").value = null;
      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-body").value = null;
      document
        .querySelector("#btn-submit-update")
        .setAttribute("disabled", true);
    });
});

//activity

//delete post
const deletePost = (id) => {
  document.querySelector(`#post-${id}`).remove();
};

// //SOLUTION
// const deletePost = (id) => {
//   fetch(`https://jsonplaceholder.typicode.com/posts${id}`, {
//     method: "DELETE",
//   }),
//     document.querySelector(`#post-${id}`).remove();
// };
